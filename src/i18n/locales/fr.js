export default {
  title: 'SOS Équipements',
  error: 'Une erreur est survenue',

  menu: {
    requests: 'Besoins',
    offers: 'Propositions',
    contact: 'Contact',
    termsOfUse: 'CGU',
  },

  home: {
    text: 'Ils sont en première ligne pour nous. Entr’aidants nous pour leur fournir le matériel dont ils manquent cruellement.',
    requests: {
      title: 'Vous avez des besoins en équipements médicaux ou en matériel de protection ?',
      text: 'Que vous soyez professionnels de santé, personnels de l’industrie alimentaire, livreurs ou toute autre personne qui se trouve en première ligne pour aider la population, ajoutez un besoin !',
      cta: 'Ajouter un besoin >',
    },
    offers: {
      title: 'Vous pouvez fournir du matériel de protection ou des équipements médicaux ?',
      text: 'Que vous soyez une entreprise qui a des stocks, un particulier qui a une imprimante 3D ou toute autre personne qui peut fournir des équipements, ajoutez une proposition !',
      cta: 'Ajouter une proposition >',
    },
  },

  offers: {
    title: 'Les Propositions',
    cta: 'Proposer des équipements',
  },

  requests: {
    title: 'Les Besoins',
    cta: 'Demander des équipements',
  },

  filters: {
    cta: 'Filtrer',
    resources: 'Offres',
  },

  list: {
    loading: 'Chargement en cours...',
    removeAll: 'Tout voir (enlever les filtres)',
    noRecords: 'Aucun résultat trouvé',
  },

  card: {
    by: 'par',
    lessInfo: 'Moins d\'info',
    moreInfo: 'Plus d\'info',
    contact: 'Contact',
  },

  severity: {
    error: 'Critique',
    warning: 'Urgent',
    info: 'Non prioritaire',
  },

  paginator: {
    sort: 'Tri :',
    previous: 'Précédent',
    next: 'Suivant',
  },

  termsOfUse: {
    title: 'CGU',
  },
};