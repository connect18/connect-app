import i18n from 'i18n-js';
import {configs} from '../_configs/configs';
import fr from './locales/fr.js';
import en from './locales/en.js';

i18n.fallbacks = true;
i18n.defaultLocale = configs.defaultLocale;
i18n.translations = {
  fr, en,
};

export default i18n;
