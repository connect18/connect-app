import React from 'react';

function CguPage() {
  return (
    <div>

      <div className="header">
        <h1>Conditions Générales d&#8242;Utilisation</h1>
      </div>


      <div className="global-content">
        
       <p>SOSEQUIPEMENTS.FR &ndash; CONDITIONS D&rsquo;UTILISATION</p>
<ol>
<li>Pr&eacute;ambule</li>
</ol>
<p>SOSEQUIPEMENTS est une initiative sociale et solidaire gratuite et temporaire dont le but est de venir en aide aux personnels mobilis&eacute;s par la gestion de la crise du virus COVID19. Les initiatives, si elles sont nombreuses, tout comme les bonnes volont&eacute;s, sont &eacute;parpill&eacute;es un peu partout sur le territoire et sont souvent m&eacute;connues. SOSEQUIPEMENTS ambitionne de centraliser le plus d&rsquo;initiatives possible pour les rendre visibles et donc efficaces.</p>
<ol>
<li>Quel est l&rsquo;objectif des conditions d&rsquo;utilisation?</li>
</ol>
<p>Les conditions d&rsquo;utilisation de SOSEQUIPEMENTS organisent les droits et obligations des utilisateurs dans le cadre de l&rsquo;utilisation des services propos&eacute;s sur https://www.SOSEQUIPEMENTS.FR</p>
<ol>
<li>Quels engagements je souscris avec SOSEQUIPEMENTS en acceptant ces conditions d&rsquo;utilisation? Et pendant combien de temps&nbsp;?</li>
</ol>
<ol>
<ol>
<li>En acceptant ces conditions d&rsquo;utilisation vous vous engagez &agrave; respecter les r&egrave;gles pos&eacute;es pour avoir le droit d&rsquo;acc&eacute;der et d&rsquo;utiliser les services propos&eacute;s sur le site web.</li>
</ol>
</ol>
<ol>
<ol>
<li>Tant que vous utilisez SOSEQUIPEMENTS.FR vous devez respectez ces conditions d&rsquo;utilisation.</li>
</ol>
</ol>
<ol>
<li>Informations re&ccedil;ues avant d&rsquo;accepter les conditions d&rsquo;utilisation.</li>
</ol>
<ol>
<ol>
<li>En acceptantles conditions d&rsquo;utilisation, vous reconnaissez que vous avez bien compris toutes les explications que vous a fournies SOSEQUIPEMENTS, via son site web ou par tout autre moyen, avant de les accepter s&rsquo;agissant de la mani&egrave;re d&rsquo;utiliser les services propos&eacute;s sur le site web.</li>
</ol>
</ol>
<ol>
<ol>
<li>En acceptant ces conditions d&rsquo;utilisation, vous reconnaissez que vous avez obtenu toutes les informations que vous souhaitiez sur le plan technique et sur l&rsquo;utilisation des services propos&eacute;s.</li>
</ol>
</ol>
<ol>
<li>Les petites annonces de SOSEQUIPEMENTS</li>
</ol>
<ol>
<ol>
<li>Avec SOSEQUIPEMENTS, vous pouvez publier des annonces permettant, dans le respect des textes l&eacute;gaux et r&eacute;glementaires, d&rsquo;&eacute;mettre des demandes de besoins d&rsquo;&eacute;quipements ou d&rsquo;en proposer.</li>
</ol>
</ol>
<ol>
<ol>
<li>Ces offres et ces demandes doivent imp&eacute;rativement &ecirc;tre gratuites et permettre directement ou indirectement d&rsquo;apporter son soutien &agrave; la lutte contre la pand&eacute;mie du COVID19.</li>
</ol>
</ol>
<ol>
<ol>
<li>Aucune offre de service payante n&rsquo;est donc autoris&eacute;e sur SOSEQUIPEMENTS.FR.</li>
</ol>
</ol>
<ol>
<ol>
<li>Toute publication contraire &agrave; l'ordre public ou aux bonnes m&oelig;urs, notamment par l&rsquo;insertion d&rsquo;&eacute;l&eacute;ments tels que, sans que cela soit exhaustif, toute publication &agrave; caract&egrave;re pornographique ou p&eacute;dophile, ou tout contenu incitant &agrave; la violence ou faisant l&rsquo;apologie du terrorisme,ou rev&ecirc;tant le caract&egrave;re d&rsquo;appel au meurtre ou d&rsquo;incitation &agrave; la haine raciale ou portant atteinte &agrave; la vie priv&eacute;e ou aux droits de la personnalit&eacute; d&rsquo;autrui donnera lieu &agrave; la mise en &oelig;uvre des voies de droit n&eacute;cessaires.</li>
</ol>
</ol>
<ol>
<ol>
<li>SOSEQUIPEMENTS.FR op&egrave;re une mod&eacute;ration des contenus publi&eacute;s, toutefois vous reconnaissez &ecirc;tre inform&eacute; qu&rsquo;en vertu de la loi n&deg; 2004-575 du 21 juin 2004&nbsp;sur la confiance dans l&rsquo;&eacute;conomie num&eacute;rique et notamment de ses articles 6-2 et 6-3 SOSEQUIPEMENTS n&rsquo;est pas soumis &agrave; <em>&laquo;&nbsp;une obligation g&eacute;n&eacute;rale de surveiller les informations qu'il transmet ou stocke, ni &agrave; une obligation g&eacute;n&eacute;rale de rechercher des faits ou des circonstances r&eacute;v&eacute;lant des activit&eacute;s illicites&nbsp;&raquo;</em>.</li>
</ol>
</ol>
<ol>
<ol>
<li>Si vous constatez la pr&eacute;sence d&rsquo;annonces susceptibles de porter atteinte aux droits des tiers, vous vous engagez donc &agrave; nous le faire savoir dans les meilleurs d&eacute;lais et conform&eacute;ment &agrave; l&rsquo;article 6.5 de la loi susvis&eacute;e.</li>
</ol>
</ol>
<ol>
<li>Devoir de vigilance des personnels b&eacute;n&eacute;ficiaire de SOSEQUIPEMENTS</li>
</ol>
<ol>
<ol>
<li>SOSEQUIPEMENTS n&rsquo;intervient qu&rsquo;en qualit&eacute; de plateforme destin&eacute;e &agrave; centraliser les offres et les demandes de services permettant de faciliter les &eacute;changes gratuits de mat&eacute;riels n&eacute;cessaires &agrave; la lutte contre la pand&eacute;mie du COVID19.</li>
</ol>
</ol>
<ol>
<ol>
<li>En sa qualit&eacute; de simple interm&eacute;diaire, SOSEQUIPEMENTS ne peut garantir la s&eacute;curit&eacute; et la fiabilit&eacute; de toutes les offres et il appartient donc aux b&eacute;n&eacute;ficiaires, comme sur tout site d&rsquo;annonces, de respecter l&rsquo;ensemble des r&egrave;gles d&eacute; s&eacute;curit&eacute; qui s&rsquo;imposent lorsque des services d&eacute;mat&eacute;rialis&eacute;s sont utilis&eacute;s&nbsp;: ne pas communiquer de mot de passe, de code (CB ou autre), assurer les v&eacute;rifications minimums etc.</li>
</ol>
</ol>
<ol>
<ol>
<li>Les d&eacute;positaires d&rsquo;annonces doivent quant &agrave; eux op&eacute;rer les v&eacute;rifications n&eacute;cessaires pour s&rsquo;assurer que le b&eacute;n&eacute;ficiaire de leur offre pr&eacute;sentent les garanties requises (qualit&eacute;s pr&eacute;tendues de professionnel d&rsquo;un corps en particulier, absence d&rsquo;objectifs mercantiles ou prohib&eacute;s) pour en &ecirc;tre destinataire.</li>
</ol>
</ol>
<ol>
<li>Ce qu&rsquo;il est indispensable d&rsquo;avoir pour faire fonctionner SOSEQUIPEMENTS.</li>
</ol>
<ol>
<ol>
<li>Pour utiliser SOSEQUIPEMENTS vous devez vous connecter &agrave; l&rsquo;Internet.</li>
</ol>
</ol>
<ol>
<ol>
<li>Cet acc&egrave;s &agrave; l&rsquo;Internet vous est fournit par des fournisseurs d&rsquo;acc&egrave;s &agrave; l&rsquo;Internet (qu&rsquo;on appelle aussi &laquo;&nbsp;FAI&nbsp;&raquo;) ou des op&eacute;rateurs de t&eacute;l&eacute;phonie mobile si vous vous connectez avec des appareils mobiles (type smartphone ou tablette).</li>
</ol>
</ol>
<ol>
<ol>
<li>Ces FAI sont les seuls responsables en cas de mauvais fonctionnement de la connexion vous permettant d&rsquo;acc&eacute;der &agrave; SOSEQUIPEMENTS.</li>
</ol>
</ol>
<ol>
<ol>
<li>Si les r&eacute;seaux qui permettent le fonctionnement de l&rsquo;Internet fonctionnent normalement, vous pouvez acc&eacute;der en permanence &agrave; SOSEQUIPEMENTS.</li>
</ol>
</ol>
<ol>
<ol>
<li>Certaines mises &agrave; jour peuvent rendre indisponible SOSEQUIPEMENTS.FR pendant de courts moments m&ecirc;me si SOSEQUIPEMENTS essaye de mettre en &oelig;uvre ces mises &agrave; jour &agrave; des horaires ne g&ecirc;nant pas l&rsquo;utilisation du site web (en fin de soir&eacute;e ou m&ecirc;me la nuit par exemple).</li>
</ol>
</ol>
<ol>
<ol>
<li>En acceptant les conditions d&rsquo;utilisation, vous renoncez donc &agrave; engager la responsabilit&eacute; de SOSEQUIPEMENTS pour ces &eacute;ventuels et ponctuels d&eacute;sagr&eacute;ments n&eacute;cessaires &agrave; l&rsquo;&eacute;volution et au bon fonctionnement des services.</li>
</ol>
</ol>
<ol>
<li>Respect des r&egrave;gles l&eacute;gales et r&egrave;glementaires</li>
</ol>
<ol>
<ol>
<li>L&rsquo;utilisation de SOSEQUIPEMENTS pour la promotion de prestations payantes est interdite.</li>
</ol>
</ol>
<ol>
<ol>
<li>Il est rappel&eacute; aux utilisateurs que <em>&laquo;&nbsp;nul n&rsquo;est cens&eacute; ignorer la loi&nbsp;&raquo;</em> et qu&rsquo;il ne rentre pas dans les missions de SOSEQUIPEMENTS de les renseigner de mani&egrave;re exhaustive sur l&rsquo;&eacute;tat du droit concernant l&rsquo;usage qu&rsquo;ils font des services mis &agrave; leur disposition.</li>
</ol>
</ol>
<ol>
<ol>
<li>Les informations juridiques fournies par SOSEQUIPEMENTS le sont donc &agrave; titre d&rsquo;avertissement et de rappel mais ne sauraient &ecirc;tre exhaustives au regard de la multiplicit&eacute; des r&egrave;gles en vigueur. La responsabilit&eacute; de SOSEQUIPEMENTS ne saurait donc &ecirc;tre recherch&eacute;e pour un d&eacute;faut d&rsquo;information juridique s&rsquo;agissant du respect du droit positif que chacun est cens&eacute; conna&icirc;tre ou rechercher avant d&rsquo;utiliser les services propos&eacute;s.</li>
</ol>
</ol>
<ol>
<ol>
<li>En cas de doute sur la conformit&eacute; d&rsquo;une annonce aux r&egrave;glementations et/ou &agrave; la l&eacute;gislation en vigueur, l'utilisateur fait donc sienne l&rsquo;obligation de v&eacute;rifier l&rsquo;&eacute;tat du droit avant de proc&eacute;der au d&eacute;p&ocirc;t de son annonce.</li>
</ol>
</ol>
<ol>
<ol>
<li>SOSEQUIPEMENTS est uniquement h&eacute;bergeur de l&rsquo;annonce et n&rsquo;op&egrave;re donc pas de contr&ocirc;le a priori de conformit&eacute; de celles-ci.</li>
</ol>
</ol>
<ol>
<ol>
<li>En publiant une annonce, vous en assumez donc la responsabilit&eacute;</li>
</ol>
</ol>
<ol>
<li>Comment contacter quelqu&rsquo;un si j&rsquo;ai besoin d&rsquo;assistance&nbsp;?</li>
</ol>
<p>Si vous avez besoin d&rsquo;assistance et que nos outils d&rsquo;aide en ligne ne suffisent pas, vous pouvez envoyer un email &agrave; <em>kevin@2ndpotion.com</em></p>
<ol>
<li>O&ugrave; sont h&eacute;berg&eacute;es les donn&eacute;es que vous mettez sur la Solution&nbsp;?</li>
</ol>
<ol>
<ol>
<li>Pour pouvoir publier des photos, des textes ou encore des films sur SOSEQUIPEMENTS, il faut utiliser un serveur.</li>
</ol>
</ol>
<ol>
<ol>
<li>Les coordonn&eacute;es de ce serveur sont les suivantes&nbsp;: <a href="https://airtable.com/">https://airtable.com/</a></li>
</ol>
</ol>
<ol>
<li>Ce que vous devez v&eacute;rifier avant de laisser un contenu &ecirc;tre publi&eacute; sur la Solution.</li>
</ol>
<ol>
<ol>
<li>SOSEQUIPEMENTS ne peut pas syst&eacute;matiquement proc&eacute;der &agrave; la v&eacute;rification des droits des tiers avant leur publication et notamment des droits de propri&eacute;t&eacute; intellectuelle. Vous garantissez donc &agrave; SOSEQUIPEMENTSque la publication des contenus respecte bien les droits de propri&eacute;t&eacute; intellectuelle de leurs auteurs et que SOSEQUIPEMENTSpourra exploiter l&rsquo;ensemble des contenus publi&eacute;s, sous r&eacute;serve du respect de l&rsquo;&eacute;ventuel droit &agrave; l&rsquo;image des sujets concern&eacute;s. Ce droit prendra fin avec le retrait des contenus concern&eacute;s, sans que cela ne remette en cause les droits accord&eacute;s ant&eacute;rieurement au(x) retrait(s).</li>
</ol>
</ol>
<ol>
<ol>
<li>Vous vous engagez &agrave; garantir SOSEQUIPEMENTS de toutes les cons&eacute;quences notamment financi&egrave;res qui d&eacute;couleraient de la violation de droits de propri&eacute;t&eacute; intellectuelle due &agrave; des d&eacute;clarations erron&eacute;es de votre part.</li>
</ol>
</ol>
<ol>
<li>Quelles sont les limitations &agrave; la responsabilit&eacute; de SOSEQUIPEMENTS?</li>
</ol>
<ol>
<ol>
<li>Pour engager la responsabilit&eacute; de SOSEQUIPEMENTS concernant le bon fonctionnement SOSEQUIPEMENTS.FR vous acceptez de devoir d&eacute;montrer qu&rsquo;elle a commis une faute&nbsp;: SOSEQUIPEMENTS est tenu &agrave; ce titre d&rsquo;une obligation de moyen.</li>
</ol>
</ol>
<ol>
<ol>
<li>En acceptant les conditions d&rsquo;utilisation, en cas de faute prouv&eacute;ede SOSEQUIPEMENTS, vous acceptez de limiter les r&eacute;parations financi&egrave;res qui vous seront dues aux dommages directs et pr&eacute;visibles occasionn&eacute;s dans le cadre de l&rsquo;utilisation de SOSEQUIPEMENTS.FR &agrave; l&rsquo;exclusion des pertes ou des dommages indirects ou impr&eacute;visibles que vous ou des tiers pourraient subir comme par exemple&nbsp;: tout gain manqu&eacute;, perte totale ou partielle, inexactitude ou corruption partielle ou totale de fichiers ou de donn&eacute;es, pr&eacute;judice commercial, perte de chiffre d'affaires ou de b&eacute;n&eacute;fice, perte de client&egrave;le, perte d'une chance, co&ucirc;t de l&rsquo;obtention d&rsquo;un produit, d&rsquo;un service ou de technologie de substitution, en relation ou provenant de l&rsquo;inex&eacute;cution ou de l&rsquo;ex&eacute;cution fautive des obligations</li>
</ol>
</ol>
<ol>
<li>Concernant la protection des Donn&eacute;es Personnelles</li>
</ol>
<ol>
<ol>
<li>Pour utiliser SOSEQUIPEMENTS, vous devez fournir des donn&eacute;es personnelles n&eacute;cessaire au fonctionnement du site web.</li>
</ol>
</ol>
<ol>
<ol>
<li>SOSEQUIPEMENTS vous informe ci-apr&egrave;s des fondements l&eacute;gaux autorisant la r&eacute;colte et le traitement de ces Donn&eacute;es Personnelles&nbsp;:</li>
</ol>
</ol>
<ul>
<li>Article 6.1.b du RGPD&nbsp;: <em>&laquo;&nbsp;le traitement est n&eacute;cessaire &agrave; l'ex&eacute;cution d'un contrat auquel la personne concern&eacute;e est partie ou &agrave; l'ex&eacute;cution de mesures pr&eacute;contractuelles prises &agrave; la demande de celle-ci; <br /> &raquo;</em></li>
<li>Article 6.1.c du RGPD&nbsp;: <em>&laquo; le traitement est n&eacute;cessaire au respect d'une obligation l&eacute;gale &agrave; laquelle le responsable du traitement est soumis; <br /> &raquo;</em></li>
</ul>
<ol>
<ol>
<li>SOSEQUIPEMENTS vous informe que ces donn&eacute;es personnelles sont destin&eacute;es &agrave; la mise en &oelig;uvre des services propos&eacute;s sur SOSEQUIPEMENTS.FR.</li>
</ol>
</ol>
<ol>
<ol>
<li>Gr&acirc;ce &agrave; la loi, vous b&eacute;n&eacute;ficiez d&rsquo;un droit d&rsquo;acc&egrave;s, de rectification, d&rsquo;opposition, de suppression et de portabilit&eacute; sur ces Donn&eacute;es Personnelles. Ces droits peuvent s&rsquo;exercer en adressant un courrier recommand&eacute; avec accus&eacute; de r&eacute;ception &agrave; kevin@2ndpotion.com.</li>
</ol>
</ol>
<ol>
<ol>
<li>Pour assurer la s&eacute;curit&eacute; de ces donn&eacute;es personnelles, toute demande doit &ecirc;tre accompagn&eacute;e d&rsquo;une copie d&rsquo;un titre d&rsquo;identit&eacute; en cours de validit&eacute; au jour de la demande.</li>
</ol>
</ol>
<ol>
<li>Dispositions contractuelles diverses.</li>
</ol>
<ol>
<ol>
<li>Si un diff&eacute;rend survient entre vous et SOSEQUIPEMENTS, les parties acceptent donc que les registres informatiques qui contiennent une trace de leurs &eacute;changes&agrave; titre de preuve entre elles. SOSEQUIPEMENTS garantit que la conservation de ces &eacute;changesest effectu&eacute;e dans des conditions de nature &agrave; en garantir l'int&eacute;grit&eacute;.</li>
</ol>
</ol>
<ol>
<ol>
<li>Si une des clause des conditions d&rsquo;utilisation, pour quelque raison que ce soit, s&rsquo;av&egrave;re inop&eacute;rante, (par exemple&nbsp;: nullit&eacute;, caducit&eacute;&hellip;), toutes les autres clauses demeurent malgr&eacute; tout valables.</li>
</ol>
</ol>
<ol>
<li>En cas de litiges entre nous, comment &ccedil;a se passe&nbsp;?</li>
</ol>
<ol>
<ol>
<li>Le seul droit qui s&rsquo;applique aux conditions d&rsquo;utilisation est le droit fran&ccedil;ais.</li>
</ol>
</ol>
<ol>
<ol>
<li>En cas de diff&eacute;rends entre les parties, celles-ci conviennent de se rencontrer dans les 30 jours de la naissance du diff&eacute;rend, dans un lieu situ&eacute; dans le ressort de la Cour d&rsquo;Appel de MONTPELLIER ou d&rsquo;organiser une r&eacute;union &agrave; distance pour &eacute;voquer les difficult&eacute;s rencontr&eacute;es.</li>
</ol>
</ol>
<ol>
<ol>
<li>Au cours de cette r&eacute;union, les parties doivent essayer de trouver une issue amiable &agrave; leur(s) diff&eacute;rend(s).</li>
</ol>
</ol>
<ol>
<ol>
<li>Cette phase de n&eacute;gociations est une obligation contractuelle.</li>
</ol>
</ol>
<ol>
<ol>
<li>Si dans les 15 jours qui suivent cette rencontre, les parties n&rsquo;ont pu se mettre d&rsquo;accord, elles peuvent alors avoir recours &agrave; une proc&eacute;dure judiciaire. Seules les juridictions du ressort de la Cour d&rsquo;Appel de MONTPELLIER peuvent &ecirc;tre saisies.</li>
</ol>
</ol>
<ol>
<ol>
<li>Aucun proc&egrave;s concernant un diff&eacute;rend li&eacute; directement ou indirectement aux conditions d&rsquo;utilisation ne peut avoir lieu plus d&rsquo;un an apr&egrave;s la naissance de la pr&eacute;tention conform&eacute;ment aux dispositions de l&rsquo;article 2254 du code civil.</li>
</ol>
</ol>
<ol>
<li>D&eacute;finitions de certains termes utilis&eacute;s dans les conditions g&eacute;n&eacute;rales.</li>
</ol>
<p>Pour &eacute;viter toute incompr&eacute;hension et toute discussion lors d&rsquo;un diff&eacute;rend, les d&eacute;finitions qui suivent fixent la signification contractuelle des termes figurantdans les conditions d&rsquo;utilisation.</p>
<p><strong>Donn&eacute;es Personnelles</strong> : au sens de l&rsquo;article 4.1 du RGPD&nbsp;: <em>&laquo;&nbsp;toute information se rapportant &agrave; une personne physique identifi&eacute;e ou identifiable (ci-apr&egrave;s d&eacute;nomm&eacute;e &laquo;personne concern&eacute;e&raquo;); est r&eacute;put&eacute;e &ecirc;tre une &laquo;personne physique identifiable&raquo; une personne physique qui peut &ecirc;tre identifi&eacute;e, directement ou indirectement, notamment par r&eacute;f&eacute;rence &agrave; un identifiant, tel qu'un nom, un num&eacute;ro d'identification, des donn&eacute;es de localisation, un identifiant </em></p>
<p><strong>Responsable de Traitement&nbsp;</strong>: au sens de l&rsquo;article 4.7 du RGPD&nbsp;: <em>&laquo;&nbsp;la personne physique ou morale, l'autorit&eacute; publique, le service ou un autre organisme qui, seul ou conjointement avec d'autres, d&eacute;termine les finalit&eacute;s et les moyens du traitement; lorsque les finalit&eacute;s et les moyens de ce traitement sont d&eacute;termin&eacute;s par le droit de l'Union ou le droit d'un &Eacute;tat membre, le responsable du traitement peut &ecirc;tre d&eacute;sign&eacute; ou les crit&egrave;res sp&eacute;cifiques applicables &agrave; sa d&eacute;signation peuvent &ecirc;tre pr&eacute;vus par le droit de l'Union ou par le droit d'un &Eacute;tat membre;&nbsp;&raquo;<br /></em></p>
<p><strong>RGPD&nbsp;</strong>: (r&egrave;glement g&eacute;n&eacute;ral sur la protection des donn&eacute;es) R&egrave;glement (UE) 2016/679 du Parlement Europ&eacute;en et du Conseil du 27 avril 2016 relatif &agrave; la protection des personnes physiques &agrave; l'&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es, et abrogeant la directive 95/46/CE</p>
<p><strong>Traitement</strong><strong>: </strong>au sens de l&rsquo;article 4.2 du RGPD&nbsp;: <em>&laquo;&nbsp;toute op&eacute;ration ou tout ensemble d'op&eacute;rations effectu&eacute;es ou non &agrave; l'aide de proc&eacute;d&eacute;s automatis&eacute;s et appliqu&eacute;es &agrave; des donn&eacute;es ou des ensembles de donn&eacute;es &agrave; caract&egrave;re personnel, telles que la collecte, l'enregistrement, l'organisation, la structuration, la conservation, l'adaptation ou la modification, l'extraction, la consultation, l'utilisation, la communication par transmission, la diffusion ou toute autre forme de mise &agrave; disposition, le rapprochement ou l'interconnexion, la limitation, l'effacement ou la destruction;&nbsp;&raquo;</em></p>


      </div>


      <div className="header">
        <h1>Mentions l&eacute;gales</h1>
      </div>

      <div>
<p><strong>Mentions l&eacute;gales</strong></p>
<p><strong>EDITEUR</strong></p>
<p>Le site web&nbsp;sosequipements.fr est &eacute;dit&eacute; par Kevin PISSARRO BRAVO, immatricul&eacute;e au Registre du Commerce et des Soci&eacute;t&eacute;s de Paris sous le SIREN 808 012 884, dont le si&egrave;ge social est situ&eacute; au 15 Square Sacha Guitry, 93420 Villepinte - T&eacute;l&eacute;phone&nbsp;: 07 70 15 36 32</p>
<p><strong>H&eacute;bergement</strong></p>
<p>Le site sosequipements.fr est h&eacute;berg&eacute; par la soci&eacute;t&eacute; soci&eacute;t&eacute; Heroku &ndash; Heroku, Inc. 650 7th Street, San Francisco, CA</p>
<p><strong>Cr&eacute;dits</strong></p>
<p>Visuels&nbsp;:<a href="https://www.freepik.com/free-vector/doctors-concept-illustration_7191136.htm%23page=1&amp;query=medical&amp;position=6">https://www.freepik.com/free-vector/doctors-concept-illustration_7191136.htm#page=1&amp;query=medical&amp;position=6</a></p>
<p><strong>PERTINENCE DES CONTENUS</strong></p>
<p>La soci&eacute;t&eacute; sosequipements.frest une initiative sociale et solidaire gratuite et temporaire dont le but est de venir en aide aux personnels mobilis&eacute;s par la gestion de la crise du virus COVID19. Les initiatives, si elles sont nombreuses, tout comme les bonnes volont&eacute;s, sont &eacute;parpill&eacute;es un peu partout sur le territoire et sont souvent m&eacute;connues. SOSEQUIPEMENTS ambitionne de centraliser le plus d&rsquo;initiatives possible pour les rendre visibles et donc efficaces.</p>
<p>Si vous constatez la pr&eacute;sence de contenusinexacts ou susceptibles de porter atteinte aux droits d&rsquo;un tiers ou violant selon vous une disposition r&eacute;glementaire ou l&eacute;gislative, merci de nous la signaler en &eacute;crivant &agrave; kevin@2ndpotion.com</p>
<p><strong>R&eacute;daction du site internet</strong></p>
<p>Directeur de la publication : Kevin PISSARRO BRAVO</p>
<p>- Responsable de la r&eacute;daction du Site : Kevin PISSARRO BRAVO<br /> Contact : 07 70 15 36 32 - kevin@2ndpotion.com</p>
<p><strong>Respect de la propri&eacute;t&eacute; intellectuelle</strong></p>
<p>Toutes les marques, photographies, textes, commentaires, illustrations, images anim&eacute;es ou non, s&eacute;quences vid&eacute;o, sons, ainsi que toutes les applications informatiques qui pourraient &ecirc;tre utilis&eacute;es pour faire fonctionner le Site et plus g&eacute;n&eacute;ralement tous les &eacute;l&eacute;ments reproduits ou utilis&eacute;s sur le Site sont prot&eacute;g&eacute;s par les lois en vigueur au titre de la propri&eacute;t&eacute; intellectuelle.</p>
<p>Ils sont la propri&eacute;t&eacute; pleine et enti&egrave;re de l'Editeur ou de ses partenaires, sauf mentions particuli&egrave;res. Toute reproduction, repr&eacute;sentation, utilisation ou adaptation, sous quelque forme que ce soit, de tout ou partie de ces &eacute;l&eacute;ments, y compris les applications informatiques, sans l'accord pr&eacute;alable et &eacute;crit de l'Editeur, sont strictement interdites. Le fait pour l'Editeur de ne pas engager de proc&eacute;dure d&egrave;s la prise de connaissance de ces utilisations non autoris&eacute;es ne vaut pas acceptation desdites utilisations et renonciation aux poursuites.</p>
<p>Seule l'utilisation pour un usage priv&eacute; dans un cercle de famille est autoris&eacute;e et toute autre utilisation est constitutive de contrefa&ccedil;on et/ou d'atteinte aux droits voisins, sanctionn&eacute;es par Code de la propri&eacute;t&eacute; intellectuelle.</p>
<p>La reprise de tout ou partie de ce contenu n&eacute;cessite l'autorisation pr&eacute;alable de l'Editeur ou du titulaire des droits sur ce contenu.</p>
<p><strong>Liens hypertextes</strong></p>
<p>Le Site peut contenir des liens hypertexte donnant acc&egrave;s &agrave; d'autres sites web &eacute;dit&eacute;s et g&eacute;r&eacute;s par des tiers et non par l'Editeur. L'Editeur ne pourra &ecirc;tre tenu responsable directement ou indirectement dans le cas o&ugrave; lesdits sites tiers ne respecteraient pas les dispositions l&eacute;gales. La cr&eacute;ation de liens hypertexte vers le Site ne peut &ecirc;tre faite qu'avec l'autorisation &eacute;crite et pr&eacute;alable de l'Editeur.</p>
<p><strong>COLLECTE DES DONNEES</strong></p>
<p>La collecte de donn&eacute;es &agrave; caract&egrave;re personnel s&rsquo;op&egrave;re dans le respect des dispositions du R&Egrave;GLEMENT (UE) 2016/679 DU PARLEMENT EUROP&Eacute;EN ET DU CONSEIL du 27 avril 2016 relatif &agrave; la protection des personnes physiques &agrave; l'&eacute;gard du traitement des donn&eacute;es &agrave; caract&egrave;re personnel et &agrave; la libre circulation de ces donn&eacute;es (RGPD), pour recueillir votre consentement aux traitements n&eacute;cessaires. Le consentement peut &agrave; tout moment &ecirc;tre retir&eacute; aussi facilement qu&rsquo;il a &eacute;t&eacute; donn&eacute; conform&eacute;ment aux exigences du RGPD.</p>
<p><strong>Les utilisations sont les suivantes : </strong></p>
<ul>
<li>Gestion du fonctionnement et optimisation du site Web</li>
<li>V&eacute;rification identification et authentification des donn&eacute;es transmises par l&rsquo;UTILISATEUR</li>
<li>Mise en &oelig;uvre d'une assistance utilisateur</li>
<li>Pr&eacute;vention et d&eacute;tection des fraudes malware et gestion des incidents de s&eacute;curit&eacute;</li>
<li>Gestion des &eacute;ventuels litiges avec les utilisateurs</li>
<li>Envoi d'informations commerciales et publicitaires en fonction des pr&eacute;f&eacute;rences de l'utilisateur</li>
</ul>
<p>Sosequipements fait tout son possible pour prot&eacute;ger les donn&eacute;es personnelles contre les alt&eacute;rations, destruction et acc&egrave;s non autoris&eacute;. Toutefois Internet n&rsquo;est pas un environnement compl&egrave;tement s&eacute;curis&eacute; et le site web ne peut pas garantir la s&eacute;curit&eacute; totale de la transmission du stockage des informations.</p>
<p><strong>Conform&eacute;ment au RGPD, les utilisateurs disposent les droits suivants :</strong></p>
<ul>
<li>Mettre &agrave; jour ou supprimer les donn&eacute;es qui les concernent (via les param&egrave;tres du compte de l'utilisateur)</li>
<li>Supprimer leur compte. Toutefois les informations partag&eacute;es avec d&rsquo;autres utilisateurs comme les publications sur des forums peuvent rester visible du public sur le SITE Web m&ecirc;me apr&egrave;s la suppression du compte.</li>
<li>Exercer leur droit d&rsquo;acc&egrave;s pour conna&icirc;tre les donn&eacute;es personnelles les concernant. Dans le cadre d&rsquo;une telle demande nous vous demanderons d&rsquo;apporter une justification de votre identit&eacute; afin d&rsquo;en v&eacute;rifier l&rsquo;exactitude. Demander la mise &agrave; jour des informations les concernant</li>
<li>Demander la suppression de leurs donn&eacute;es &agrave; caract&egrave;re personnel</li>
</ul>
<p>Pour cela, envoyez un mail &agrave; kevin@2ndpotion.com</p>
<p><strong>COOKIES</strong></p>
<p>Le site peut collecter automatiquement des informations standard telles que tous types d'informations personnalis&eacute;es qui permettent au site d'identifier ses visiteurs. <br /><br /> Toutes les informations collect&eacute;es indirectement ne seront utilis&eacute;es que pour suivre le volume, le type et la configuration du trafic utilisant ce site, pour en d&eacute;velopper la conception et l'agencement et &agrave; d'autres fins administratives et de planification et plus g&eacute;n&eacute;ralement pour am&eacute;liorer le service que nous vous offrons. <br /><br /> Vous avez la possibilit&eacute; de refuser la pr&eacute;sence des cookies sur votre ordinateur</p>


      </div>


    </div>

    );
}

export default CguPage;