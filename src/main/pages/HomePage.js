import React from 'react';
import i18n from '../../i18n/i18n';
import {Link} from 'react-router-dom';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';
import {URLS} from '../../_configs/urls';
import './_css/homePage.css';

function HomePage() {
  return (
    <div>
      <header className="Home_header">
        <img height="200" src="doctors.svg" alt="Le personnel en première ligne"/>
        <h1>{i18n.t('title')}</h1>
        <p>
          {i18n.t('home.text')}
        </p>
      </header>
      <section className="Home_section">
        <article className="Home_needs">
          <h2><WbIncandescentIcon/> {i18n.t('home.requests.title')}</h2>
          <p>{i18n.t('home.requests.text')}</p>
          <div className="Home_link">
            <Link to={URLS.requestsForm}>{i18n.t('home.requests.cta')}</Link>
          </div>
        </article>

        <article className="Home_offers">
          <h2><SettingsApplicationsIcon/> {i18n.t('home.offers.title')}
          </h2>
          <p>{i18n.t('home.offers.text')}</p>
          <div className="Home_link">
            <Link to={URLS.offersForm}>{i18n.t('home.offers.cta')}</Link>
          </div>
        </article>
      </section>
    </div>
  );
}

export default HomePage;