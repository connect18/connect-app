import React, {useMemo} from 'react';
import {configs} from '../../_configs/configs';
import Button from '@material-ui/core/Button';
import {Box} from '@material-ui/core';
import {URLS} from '../../_configs/urls';
import {useUrlFilters} from '../../filters/useUrlFilters';
import Filters from '../../filters/Filters';
import ListStore from '../../common/lists/ListStore';
import CardsList from '../../common/cards/CardsList';
import Card from '../../common/cards/Card';
import i18n from '../../i18n/i18n';
import {Link} from 'react-router-dom';

const FIELDS = [
  'Autre', 'Quantités', 'Type de don', 'Type de matériel', 'Commentaire', 'Matériel disponible pour la conception',
  'Autre moyen de conception', 'Statut', 'Adresse de récupération', 'Prénom',
];

function OffersPage() {
  const filters = useUrlFilters();
  const listStore = useMemo(
    () => new ListStore(
      configs.airtable.offers.baseId, configs.airtable.offers.tableId, configs.airtable.offers.resourcesFieldId,
      filters,
    ),
    [filters],
  );

  return (
    <div>
      <h1 className="flex_row_center justifyContent_spaceBetween flexWrap_wrap">
        <Box mr={1}>{i18n.t('offers.title')}</Box>
        <Link to={URLS.offersForm} style={{textDecoration: 'none'}}>
          <Button
            color="primary"
            variant="contained"
          >
            {i18n.t('offers.cta')}
          </Button>
        </Link>
      </h1>
      <Box my={5}>
        <Filters listStore={listStore}/>
        <CardsList
          headers={[{id: 'Département', label: 'département'}, {id: 'Commune', label: 'ville'}]}
          listStore={listStore}
          renderRecord={(record, index) => (
            <Card
              color="primary"
              resourcesFieldId={configs.airtable.offers.resourcesFieldId}
              record={record}
              index={index} fields={FIELDS}
            />
          )}
        />
      </Box>
    </div>
  );
}

export default OffersPage;