import React, {useMemo} from 'react';
import Button from '@material-ui/core/Button';
import {Box} from '@material-ui/core';
import Card from '../../common/cards/Card';
import {useUrlFilters} from '../../filters/useUrlFilters';
import {configs} from '../../_configs/configs';
import ListStore from '../../common/lists/ListStore';
import {URLS} from '../../_configs/urls';
import Filters from '../../filters/Filters';
import CardsList from '../../common/cards/CardsList';
import i18n from '../../i18n/i18n';
import {Link} from 'react-router-dom';

const FIELDS = ['Autre', 'Quantités', 'Commentaire', 'Profession', 'Statut', 'Adresse de livraison', 'Prénom'];

function RequestsPage() {
  const filters = useUrlFilters();
  const listStore = useMemo(
    () => new ListStore(
      configs.airtable.requests.baseId, configs.airtable.requests.tableId, configs.airtable.requests.resourcesFieldId,
      filters,
    ),
    [],
  );
  return (
    <div>
      <h1 className="flex_row_center justifyContent_spaceBetween flexWrap_wrap">
        <Box mr={1}>{i18n.t('requests.title')}</Box>
        <Link to={URLS.requestsForm} style={{textDecoration: 'none'}}>
          <Button
            color="secondary"
            variant="contained"
          >
            {i18n.t('requests.cta')}
          </Button>
        </Link>
      </h1>
      <Box my={5}>
        <Filters listStore={listStore}/>
        <CardsList
          headers={[{id: 'Département', label: 'département'}, {id: 'Commune', label: 'ville'}]}
          listStore={listStore}
          renderRecord={(record, index) => (
            <Card
              color="secondary"
              resourcesFieldId={configs.airtable.requests.resourcesFieldId}
              record={record}
              index={index}
              fields={FIELDS}
            />
          )}
        />
      </Box>
    </div>
  );
}

export default RequestsPage;