import React from 'react';

function FormPage(props) {
  return (
    <div style={{position: 'fixed', left: 0, right: 0, top: 60, bottom: 0}}>
      <iframe
        src={props.src}
        frameBorder="0"
        width="100%"
        height="100%"
      />
    </div>
  );
}

export default FormPage;