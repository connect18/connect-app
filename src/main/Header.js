import React, {useState} from 'react';
import Container from '@material-ui/core/Container';
import {Box, Icon, Drawer, List, ListItem} from '@material-ui/core';
import {NavLink} from 'react-router-dom';
import WbIncandescentIcon from '@material-ui/icons/WbIncandescent';
import SettingsApplicationsIcon from '@material-ui/icons/SettingsApplications';
import DescriptionIcon from '@material-ui/icons/Description';
import EmailIcon from '@material-ui/icons/Email';
import './_css/header.css';
import {URLS} from '../_configs/urls';
import i18n from '../i18n/i18n';

function Header() {
  const [drawer, setDrawer] = useState(false);

  function toggleDrawer() {
    setDrawer(!drawer);
  }

  return (
    <div className="Header-wrapper">
      <header className="Header">
        <Container maxWidth="lg">
          <Box className="flex_row_center justifyContent_spaceBetween">
            <NavLink className={i18n.t('title')} to={URLS.home}>
              <img
                width="200px"
                src="logo-sos-equipements-412x60.png"
                alt={i18n.t('title')}
              />
            </NavLink>
            <div className="flex_row_center Header_navigation">
              <NavLink to={URLS.requests} activeClassName='Header_active'>
                <WbIncandescentIcon className="Header_icon"/> {i18n.t('menu.requests')}
              </NavLink>
              <NavLink to={URLS.offers} activeClassName='Header_active'>
                <SettingsApplicationsIcon className="Header_icon"/> {i18n.t('menu.offers')}
              </NavLink>
              <NavLink to={URLS.contact} activeClassName='Header_active'>
                <EmailIcon className="Header_icon"/> {i18n.t('menu.contact')}
              </NavLink>
              <NavLink to={URLS.cgu} activeClassName='Header_active'>
                <DescriptionIcon className="Header_icon"/> {i18n.t('menu.termsOfUse')}
              </NavLink>
            </div>
            <div className="flex_row_center Header_btn">
              <Icon style={{color: 'white'}} onClick={toggleDrawer}>menu</Icon>
            </div>
          </Box>
        </Container>
        <Drawer anchor="right" open={drawer} onClose={toggleDrawer}>
          <List className="Drawer">
            <ListItem>
              <NavLink to={URLS.requests}>
                <WbIncandescentIcon className="Header_icon"/> {i18n.t('menu.requests')}</NavLink>
            </ListItem>
            <ListItem>
              <NavLink to={URLS.offers}>
                <SettingsApplicationsIcon className="Header_icon"/> {i18n.t('menu.offers')}
              </NavLink>
            </ListItem>
            <ListItem>
              <NavLink to={URLS.contact}><EmailIcon className="Header_icon"/> {i18n.t('menu.contact')}</NavLink>
            </ListItem>
            <ListItem>
              <NavLink to={URLS.cgu}><DescriptionIcon className="Header_icon"/> {i18n.t('menu.termsOfUse')}</NavLink>
            </ListItem>
          </List>
        </Drawer>
      </header>
    </div>
  );
}

export default Header;