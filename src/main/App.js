import React from 'react';
import {BrowserRouter, Redirect, Route, Switch} from 'react-router-dom';
import {URLS} from '../_configs/urls';
import HomePage from './pages/HomePage';
import Container from '@material-ui/core/Container';
import Header from './Header';
import CguPage from './pages/CguPage';
import {createMuiTheme, ThemeProvider} from '@material-ui/core/styles';
import FormPage from './pages/FormPage';
import {configs} from '../_configs/configs';
import OffersPage from './pages/OffersPage';
import RequestsPage from './pages/RequestsPage';

const theme = createMuiTheme({
  palette: {
    primary: {
      light: '#F2C1FF',
      main: '#9550BF',
      dark: '#633680',
    },
    secondary: {
      light: '#96FFF7',
      main: '#3BBFB4',
      dark: '#18736B',
    },
  },
});

function App() {
  return (
    <div className="App">
      <ThemeProvider theme={theme}>
        <BrowserRouter>
          <Header/>
          <Container maxWidth='lg'>
            <Switch>
              <Route
                path={URLS.contact}
                component={() => <FormPage src={configs.contact.formUrl}/>}
              />
              <Route path={URLS.cgu} component={CguPage}/>

              <Route
                path={URLS.offersForm}
                component={() => <FormPage src={configs.airtable.offers.formUrl}/>}
              />
              <Route path={URLS.offers} component={OffersPage}/>

              <Route
                path={URLS.requestsForm}
                component={() => <FormPage src={configs.airtable.requests.formUrl}/>}
              />
              <Route path={URLS.requests} component={RequestsPage}/>

              <Route path={URLS.home} component={HomePage}/>
              <Redirect to={URLS.home}/>
            </Switch>
          </Container>
        </BrowserRouter>
      </ThemeProvider>
    </div>
  );
}

export default App;
