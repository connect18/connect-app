const readResponse = function (response) {
  if (response.headers.get('content-length') === '0') {
    return Promise.resolve({});
  }

  const contentType = response.headers.get('content-type');
  let reader = 'text';
  if (contentType) {
    if (contentType.startsWith('application/json')) {
      reader = 'json';
    }
    else if (contentType.startsWith('multipart/form-data')) {
      reader = 'formData';
    }
  }
  return response[reader]();
};

const executeRequest = function (url, fetchOptions) {
  return fetch(url, fetchOptions)
    .then(
      (response) => readResponse(response).then(
        (data) => ({data, response}), () => ({data: null, response})),
      () => {
        // eslint-disable-next-line
        throw {error: 'global.errors.network'};
      },
    )
    .then(
      ({data, response}) => {
        if (data) {
          if (data.error !== undefined) { // generic API errors
            throw {key: 'errors.api.' + data.error.key};
          }
          if (response.status >= 400) {
            throw {error: 'global.errors.unknownLight'};
          }
        }
        return {data, response};
      },
    );
};


export {executeRequest};
