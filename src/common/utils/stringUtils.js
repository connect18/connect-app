const nonLetterRegex = /\s'-/;

export function capitalize(str) {
  if (!str) return '';
  let capitalizedStr = '';
  const strLowerCase = str.trim().toLocaleLowerCase();
  let capitalizeNext = true;
  for (const char of strLowerCase) {
    const nonLetter = nonLetterRegex.test(char);
    if (nonLetter) {
      capitalizeNext = true;
      capitalizedStr += char;
    } else if (capitalizeNext) {
      capitalizeNext = false;
      capitalizedStr += char.toLocaleUpperCase();
    } else {
      capitalizeNext = false;
      capitalizedStr += char;
    }
  }
  return capitalizedStr;
}