import React from 'react';
import Button from '@material-ui/core/Button';
import {observer} from 'mobx-react';
import {Box} from '@material-ui/core';
import './_css/listPaginator.css';
import ListHeader from './ListHeader';
import i18n from '../../i18n/i18n';

function ListPaginator(props) {
  const currentPage = props.listStore.currentPage.get();
  const pageData = props.listStore.pageData.get();

  return (
    <Box className='ListPaginator flexWrap_wrap'>
      <div className='flex_row_center'>
        {props.headers && <div style={{fontSize: 14, marginRight: 10}}>{i18n.t('paginator.sort')}</div>}
        {(props.headers || []).map((header) => (
          <ListHeader
            key={header.id}
            header={header}
            listStore={props.listStore}
          />
        ))}
      </div>
      <div className='flex_row_center'>
        <Button
          color="secondary"
          disabled={currentPage === 1}
          onClick={() => props.listStore.previous()}
        >
          {i18n.t('paginator.previous')}
        </Button>
        <Box component='span' ml={2}>
          <Button
            color="secondary"
            disabled={!pageData || !pageData.value || !pageData.value.offset}
            onClick={() => props.listStore.next()}
          >
            {i18n.t('paginator.next')}
          </Button>
        </Box>
      </div>
    </Box>
  );
}

export default observer(ListPaginator);