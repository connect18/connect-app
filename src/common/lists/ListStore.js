import {action, observable} from 'mobx';
import {executeRequest} from '../utils/fetchUtils';
import {configs} from '../../_configs/configs';
import {fromPromise} from 'mobx-utils';

class ListStore {

  _baseId;
  _tableId;

  paginatesItems = {};
  currentPage = observable.box(1);
  pageData = observable.box(undefined);
  sort = observable.box(undefined);
  filters;

  resourcesFieldId;
  pageSize = 20;

  constructor(baseId, tableId, resourcesFieldId, filters) {
    this._baseId = baseId;
    this._tableId = tableId;
    this.resourcesFieldId = resourcesFieldId;
    this.filters = observable.array(filters || []);
    this._loadPage(this.currentPage.get());
  }

  next = action(() => {
    this.currentPage.set(this.currentPage.get() + 1);
    this._loadPage();
  });

  previous = action(() => {
    this.currentPage.set(this.currentPage.get() - 1);
    this._loadPage();
  });

  setSort = action((sort) => {
    this.sort.set(sort);
    this._reset();
  });

  addFilter = action((filter) => {
    this.filters.push(filter);
    this._reset();
  });

  updateFilter = action((filter, filterIndex) => {
    this.filters.splice(filterIndex, 1);
    this.filters.push(filter);
    this._reset();
  });

  removeFilter = action((filterIndex) => {
    this.filters.splice(filterIndex, 1);
    this._reset();
  });

  removeAllFilters = action(() => {
    this.filters.replace([]);
    this._reset();
  });

  _reset() {
    this.currentPage.set(1);
    this.pageData.set(undefined);
    this.paginatesItems = {};
    this._loadPage();
  }

  _loadPage() {
    const page = this.currentPage.get();
    let promise;
    if (this.paginatesItems[page]) {
      promise = Promise.resolve(this.paginatesItems[page]);
    } else {
      const url = this._buildUrl();
      promise = executeRequest(url).then(({data}) => {
        this.paginatesItems[page] = data;
        return data;
      });
    }
    this.pageData.set(fromPromise(promise));
  }

  _buildUrl() {
    const page = this.currentPage.get();
    const sort = this.sort.get();
    let url = `${configs.apiUrl}/v0/${this._baseId}/${this._tableId}?pageSize=${this.pageSize}&view=Grid%20view`;

    if (page > 1) {
      url += '&offset=' + this.paginatesItems[page - 1].offset;
    }

    if (sort) {
      url += '&' + encodeURIComponent('sort[0][field]') + '=' + encodeURIComponent(sort.id);
      url += '&' + encodeURIComponent('sort[0][direction]') + '=' + encodeURIComponent(sort.direction);
    }
    const sortIndex = sort ? 1 : 0;
    url += '&' + encodeURIComponent('sort[' + sortIndex + '][field]') + '=' + encodeURIComponent('ID');
    url += '&' + encodeURIComponent('sort[' + sortIndex + '][direction]') + '=' + encodeURIComponent('desc');

    if (this.filters.length >= 0) {
      url += '&filterByFormula=' +
        encodeURIComponent('AND(' + this.filters.map((filter) => filter.formula).join(',') + ')');
    }
    return url;
  }
}

export default ListStore;