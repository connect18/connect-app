import React from 'react';
import {observer} from 'mobx-react';
import Icon from '@material-ui/core/Icon';

function ListHeader(props) {
  const sort = props.listStore.sort.get();
  const isSorted = sort && sort.id === props.header.id;
  return (
    <div
      className='flex_row_center'
      style={{cursor: 'pointer', margin: '0 10px', userSelect: 'none', opacity: isSorted ? 1 : 0.6, fontSize: 14}}
      onClick={
        () => props.listStore.setSort(
          {id: props.header.id, direction: !isSorted || sort.direction === 'desc' ? 'asc' : 'desc'},
        )
      }
    >
      <div style={{textDecoration: 'underline', textTransform: 'capitalize'}}>{props.header.label}</div>
      <Icon
        style={{
          transition: 'all 0.3s',
          transform: isSorted && sort.direction === 'desc' ? 'rotate(180deg)' : '',
          opacity: isSorted ? 1 : 0,
        }}
      >
        arrow_drop_down
      </Icon>
    </div>
  );
}

export default observer(ListHeader);