import React from 'react';
import Alert from '@material-ui/lab/Alert';
import i18n from '../../i18n/i18n';

const SEVERITIES = {
  Critique: 'error',
  Urgent: 'warning',
  'Non prioritaire': 'info',
};

export default function UrgenceAlert(props) {
  const severity = SEVERITIES[props.urgency];
  return <Alert severity={severity}>{i18n.t('severity.' + severity)}</Alert>;
}