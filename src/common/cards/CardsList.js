import React from 'react';
import {observer} from 'mobx-react';
import {Box} from '@material-ui/core';
import Button from '@material-ui/core/Button';
import ListPaginator from '../lists/ListPaginator';
import i18n from '../../i18n/i18n';

function CardsList(props) {
  const pageData = props.listStore.pageData.get();

  return (
    <div>
      <ListPaginator
        listStore={props.listStore}
        headers={props.headers}
      />
      {
        pageData && pageData.case({
          pending: () => (
            <p>{i18n.t('list.loading')}</p>
          ),
          fulfilled: (data) => {
            const records = data.records.filter((record) => Object.keys(record.fields).length > 1);
            return records.length === 0
              ? (
                <Box my={5}>
                  {i18n.t('list.noRecords')}
                  {props.listStore.filters.length && (
                    <Box mt={2}>
                      <Button
                        color="secondary"
                        variant="contained"
                        onClick={() => props.listStore.removeAllFilters()}
                      >
                        {i18n.t('list.removeAll')}
                      </Button>
                    </Box>
                  )}
                </Box>
              )
              : records.map((record, index) => (
                <div key={record.id}>
                  {props.renderRecord(record, index)}
                </div>
              ));
          },
          rejected: () => (
            <p>{i18n.t('error')}</p>
          ),
        })
      }
    </div>
  );
}

export default observer(CardsList);