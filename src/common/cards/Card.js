import React, {useState} from 'react';
import {Chip, Collapse} from '@material-ui/core';
import './_css/card.css';
import cn from 'classnames';
import {capitalize} from '../../common/utils/stringUtils';
import Button from '@material-ui/core/Button';
import Icon from '@material-ui/core/Icon';
import Grid from '@material-ui/core/Grid';
import UrgenceAlert from './UrgenceAlert';
import {RESOURCES, RESOURCES_COLORS} from '../../_configs/data/resources';
import i18n from '../../i18n/i18n';

function Card(props) {
  const [expanded, setExpanded] = useState(false);
  const fields = props.record.fields;
  return (
    <div className={cn('Card', {'Card_odd': props.index % 2})}>
      <div className="Card_normal" onClick={() => setExpanded(!expanded)}>
        <div className="flex_row_center justifyContent_spaceBetween flexWrap_wrap">
          <div>
            <div className="Card_title flex_row_center">
              <Icon style={{fontSize: 20, marginRight: 5}}>place</Icon>
              <div>{capitalize(fields['Commune'])}</div>
              <div className="Card_owner">
                {i18n.t('card.by')} {fields['Prénom']} {fields['Nom'] ? fields['Nom'][0] : ''}.
              </div>
            </div>
            <div title={fields['Département']} className="Card_title_number">
              {fields['Département']}
            </div>
          </div>
          <div style={{textAlign: 'right'}}>
            {
              fields['Urgence de la demande'] &&
              <UrgenceAlert urgency={fields['Urgence de la demande']}/>
            }
            <Button color={props.color}>{expanded ? i18n.t('card.lessInfo') : i18n.t('card.moreInfo')}</Button>
          </div>
        </div>
        <div className="flex_row_center justifyContent_spaceBetween flexWrap_wrap">
          <div className="Card_offers">
            {
              (fields[props.resourcesFieldId] || [])
                .sort((o1, o2) => RESOURCES.indexOf(o1) - RESOURCES.indexOf(o2))
                .map((offer, key) => {
                  offer = offer.trim();
                  return (
                    <Chip
                      key={key}
                      label={offer}
                      style={{backgroundColor: RESOURCES_COLORS[offer] || '#D3D3D3'}}
                      className="Card_offer"
                    />
                  );
                })
            }
          </div>
        </div>
      </div>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Grid container spacing={3} className='Card_content'>
          {
            props.fields
              .filter((field) => !!fields[field])
              .map((field) => (
                <Grid item xs={12} sm={6} key={field}>
                  <div style={{marginBottom: 5, fontWeight: 'bold', fontSize: 12}}>{field}</div>
                  {fields[field]}
                </Grid>
              ))
          }
          <Grid item xs={12} sm={6}>
            <Button
              color={props.color}
              href={`mailto:${fields['Email']}`}
              startIcon={<Icon>email</Icon>}
            >
              {fields['Email']}
            </Button>
          </Grid>
          {
            fields['Numéro de télephone'] &&
            <Grid item xs={12} sm={6}>
              <Button
                color={props.color}
                href={`tel:${fields['Numéro de télephone']}`}
                startIcon={<Icon>phone</Icon>}
              >
                {fields['Numéro de télephone']}
              </Button>
            </Grid>
          }
        </Grid>
      </Collapse>
    </div>
  );
}

export default Card;
