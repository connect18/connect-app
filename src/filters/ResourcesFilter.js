import React from 'react';
import Checkbox from '@material-ui/core/Checkbox';
import {observer} from 'mobx-react';
import {RESOURCES, RESOURCES_COLORS} from '../_configs/data/resources';
import {Chip} from '@material-ui/core';
import {buildResourcesFilter} from './filtersUtils';

function handleClick(listStore, filter, filterIndex, offer) {
  let newValue = filter ? filter.value : [];
  const checked = newValue.includes(offer);
  if (checked) {
    newValue = newValue.filter((filteredOffer) => filteredOffer !== offer);
  } else {
    newValue.push(offer);
  }

  if (newValue.length === 0) {
    listStore.removeFilter(filterIndex);
  } else {
    const newFilter = buildResourcesFilter(listStore.resourcesFieldId, newValue);
    if (filter) {
      listStore.updateFilter(newFilter, filterIndex);
    } else {
      listStore.addFilter(newFilter);
    }
  }
}

function ResourcesFilter(props) {
  const filterIndex = props.listStore.filters.findIndex(filter => filter.type === 'Resources');
  const filter = props.listStore.filters[filterIndex];
  return (
    <div>
      {RESOURCES.map((offer) => (
        <div key={offer} className='flex_row_center'>
          <Checkbox
            checked={!!filter && filter.value.includes(offer)}
            onChange={() => handleClick(props.listStore, filter, filterIndex, offer)}
          />
          <Chip
            label={offer}
            onClick={() => handleClick(props.listStore, filter, filterIndex, offer)}
            style={{margin: '3px', backgroundColor: RESOURCES_COLORS[offer] || '#D3D3D3'}}
          />
        </div>
      ))}
    </div>
  );
}

export default observer(ResourcesFilter);