export function buildRegionFilter(region) {
  return {type: 'Region', value: region, formula: '{Région} = \'' + region + '\''};
}

export function buildDepartementFilter(departement) {
  return {type: 'Departement', value: departement, formula: '{Département} = \'' + departement + '\''};
}

export function buildResourcesFilter(fieldId, resources) {
  return {
    type: 'Resources',
    value: resources,
    formula: 'OR(' + resources.map((filteredOffer) => 'SEARCH(\'' + filteredOffer + '\', ARRAYJOIN({' + fieldId + '}))') + ')',
  };
}