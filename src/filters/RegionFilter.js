import React from 'react';
import {observer} from 'mobx-react';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import {Box} from '@material-ui/core';
import {REGIONS} from '../_configs/data/regions';
import {buildRegionFilter} from './filtersUtils';

function RegionFilter(props) {
  const filterIndex = props.listStore.filters.findIndex(filter => filter.type === 'Region');
  const filter = props.listStore.filters[filterIndex];
  return (
    <Box mb={1}>
      <Box mb={0.5}>
        <InputLabel id="labelRegion">Région</InputLabel>
      </Box>
      <Select
        labelId="labelRegion"
        id="region"
        value={(filter && filter.value) || ''}
        style={{width: 250}}
        onChange={(event) => {
          const value = event.target.value;
          if (value) {
            const newFilter = buildRegionFilter(value);
            if (!filter) {
              props.listStore.addFilter(newFilter);
            } else {
              props.listStore.updateFilter(newFilter, filterIndex);
            }
          } else {
            props.listStore.removeFilter(filterIndex);
          }
        }}
      >
        <MenuItem value=''>Toutes</MenuItem>
        {REGIONS.map((region) => (
          <MenuItem key={region} value={region}>{region}</MenuItem>
        ))}
      </Select>
    </Box>
  );
}

export default observer(RegionFilter);