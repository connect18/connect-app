import {useLocation} from 'react-router';
import queryString from 'query-string';
import {buildDepartementFilter, buildResourcesFilter, buildRegionFilter} from './filtersUtils';
import {configs} from '../_configs/configs';

export function useUrlFilters() {
  const location = useLocation();
  const queries = queryString.parse(location.search);
  const filters = [];
  for (const field in queries) {
    if (field === 'region') {
      filters.push(buildRegionFilter(queries[field]));
    } else if (field === 'departement') {
      filters.push(buildDepartementFilter(queries[field]));
    } else if (field === 'offers') {
      const offers = queries[field].split(',');
      if (offers && offers.length > 0) {
        filters.push(buildResourcesFilter(configs.airtable.offers.resourcesFieldId, offers));
      }
    } else if (field === 'needs') {
      const offers = queries[field].split(',');
      if (offers && offers.length > 0) {
        filters.push(buildResourcesFilter(configs.airtable.requests.resourcesFieldId, offers));
      }
    }
  }
  return filters;
}