import React, {useState} from 'react';
import {Box, Collapse} from '@material-ui/core';
import RegionFilter from './RegionFilter';
import Grid from '@material-ui/core/Grid';
import ResourcesFilter from './ResourcesFilter';
import InputLabel from '@material-ui/core/InputLabel';
import DepartementFilter from './DepartementFilter';
import Icon from '@material-ui/core/Icon';
import Chip from '@material-ui/core/Chip';
import {observer} from 'mobx-react';
import i18n from '../i18n/i18n';

function Filters(props) {
  const [expanded, setExpanded] = useState(false);
  const ifFiltered = props.listStore.filters.length;

  return (
    <Box>
      <div
        onClick={() => setExpanded(!expanded)}
        style={{cursor: 'pointer', marginBottom: 20, userSelect: 'none', display: 'inline-block'}}
      >
        <Chip
          style={{padding: 5}}
          icon={<Icon style={{fontSize: 20}}>filter_list</Icon>}
          label={i18n.t('filters.cta')}
          clickable
          onDelete={ifFiltered ? () => {
          } : null}
          deleteIcon={
            ifFiltered
              ? <div className='flex_row_center' style={{
                borderRadius: '50%',
                backgroundColor: 'red',
                color: '#FFF',
                fontWeight: 900,
                fontSite: 14,
                justifyContent: 'center',
              }}>{props.listStore.filters.length}</div>
              : null
          }
        />
      </div>
      <Collapse in={expanded} timeout="auto" unmountOnExit>
        <Grid container spacing={3}>
          <Grid item xs={12} sm={6}>
            <Box mb={4}>
              <RegionFilter listStore={props.listStore}/>
            </Box>
            <Box mb={4}>
              <DepartementFilter listStore={props.listStore}/>
            </Box>
          </Grid>
          <Grid item xs={12} sm={6}>
            <Box mb={0.5}>
              <InputLabel>{i18n.t('filters.resources')}</InputLabel>
            </Box>
            <Box style={{maxHeight: 180, overflow: 'auto'}} mb={2}>
              <ResourcesFilter listStore={props.listStore} fieldId={props.listStore.resourcesFieldId}/>
            </Box>
          </Grid>
        </Grid>
      </Collapse>
    </Box>
  );
}

export default observer(Filters);