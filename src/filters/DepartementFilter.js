import React from 'react';
import {observer} from 'mobx-react';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import {Box} from '@material-ui/core';
import {DEPARTEMENTS} from '../_configs/data/departements';
import {buildDepartementFilter} from './filtersUtils';

function DepartementFilter(props) {
  const filterIndex = props.listStore.filters.findIndex(filter => filter.type === 'Departement');
  const filter = props.listStore.filters[filterIndex];
  return (
    <Box mb={1}>
      <Box mb={0.5}>
        <InputLabel id="labelDepartement">Département</InputLabel>
      </Box>
      <Select
        labelId="labelDepartement"
        id="departement"
        value={(filter && filter.value) || ''}
        style={{width: 250}}
        onChange={(event) => {
          const value = event.target.value;
          if (value) {
            const newFilter = buildDepartementFilter(value);
            if (!filter) {
              props.listStore.addFilter(newFilter);
            } else {
              props.listStore.updateFilter(newFilter, filterIndex);
            }
          } else {
            props.listStore.removeFilter(filterIndex);
          }
        }}
      >
        <MenuItem value=''>Tous</MenuItem>
        {DEPARTEMENTS.map((departement) => (
          <MenuItem key={departement} value={departement}>{departement}</MenuItem>
        ))}
      </Select>
    </Box>
  );
}

export default observer(DepartementFilter);