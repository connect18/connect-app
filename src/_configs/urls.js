export const URLS = {
  home: '/',
  requests: '/requests',
  requestsForm: '/requests/form',
  offers: '/offers',
  offersForm: '/offers/form',
  contact: '/contact',
  cgu: '/cgu'
};