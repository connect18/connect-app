const isProd = process.env.NODE_ENV === 'production';

export const configs = {
  env: process.env.NODE_ENV,
  apiUrl: isProd ? 'https://sosequipements-back.herokuapp.com' : 'http://localhost:4000',
  defaultLocale: 'fr',

  airtable: {
    requests: {
      baseId: isProd ? 'appfYJB5jH9fYYiKG' : 'appO9DRHiLaZaUxfG',
      tableId: 'besoins',
      resourcesFieldId: 'Votre besoin',
      formUrl: isProd
        ? 'https://airtable.com/embed/shriEfpDUcZf38Bmt?backgroundColor=teal'
        : 'https://airtable.com/embed/shrno4K4WWrJDdiEu?backgroundColor=teal',
    },

    offers: {
      baseId: isProd ? 'appfYJB5jH9fYYiKG' : 'appO9DRHiLaZaUxfG',
      tableId: 'propositions',
      resourcesFieldId: 'Vous pouvez offrir',
      formUrl: isProd
        ? 'https://airtable.com/embed/shr2V4jMVYF1Oz5xj?backgroundColor=pink'
        : 'https://airtable.com/embed/shrc6dx7ijHKNxayT?backgroundColor=pink',
    },
  },

  contact: {
    formUrl: 'https://airtable.com/embed/shrmI5hlDwXaWFvVZ?backgroundColor=red',
  },
};